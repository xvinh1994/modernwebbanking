﻿using ModernWeb.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Optimization;
using System.Web.Caching;
using System.Net;
using System.Web.Http;

namespace ModernWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Additional codes
            RegisterCacheEntry();
        }
        private bool RegisterCacheEntry()
        {
            if (HttpContext.Current.Cache["CacheForAutoSendMail"] != null) return false;

            HttpContext.Current.Cache.Add("CacheForAutoSendMail", "TestAutoSendMail", null, DateTime.MaxValue, TimeSpan.FromMinutes(2),
                CacheItemPriority.Normal, new CacheItemRemovedCallback(CacheRemovedCallback));

            return true;
        }

        private void CacheRemovedCallback(string key, object value, CacheItemRemovedReason reason)
        {
            //if this is removed <=> 2 min passed => send email
            ModernWeb.HelperClasses.NotifyHelper helper = new HelperClasses.NotifyHelper();
            System.Threading.Thread t_transfer = new System.Threading.Thread(new System.Threading.ThreadStart(helper.SendEmailToUserWithFailedTransfers));
            t_transfer.Start();
            System.Threading.Thread t_loan = new System.Threading.Thread(new System.Threading.ThreadStart(helper.SendEmailToUserWithFailedLoans));
            t_loan.Start();

            //create new cache and wait for new send mail action
            RefreshCacheByRequest();
        }

        private void RefreshCacheByRequest()
        {
            WebClient virtualBrowser = new WebClient();
            virtualBrowser.DownloadData(System.Web.Hosting.HostingEnvironment.MapPath("~/css/style.css"));
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            Session["roleID"] = 0;//anonymous
            Session["registeredID"] = 0;//anonymous

            Session["curRemittanceID"] = null;//current remittance order
            Session["transferFormChecked"] = false;//no appending remittance order

            Session["appendLoanTypeChoosing"] = null;//no appending loan type 
            Session["appendLoanPersonalInfo"] = null;//no appending loan's personal info registration
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Url.ToString() == System.Web.Hosting.HostingEnvironment.MapPath("~/css/style.css"))
            {
                RegisterCacheEntry();
            }
        }
    }
}