﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ModernWeb.Models;
using System.Data.SqlClient;

namespace ModernWeb.HelperClasses
{
    public class DBHelper
    {
        public int KiemTraChuyenKhoanGianDoan(int registeredID)
        {
            BankDBEntities db = new BankDBEntities();
            //lấy hết các chuyển khoản chưa đc ktra
            List<RemittanceRecord> undoneRecords = db.RemittanceRecords.Where(r => r.RegisteredID == registeredID && r.StatusID == 1).ToList();

            for (int i = 0; i < undoneRecords.Count(); i++)
            {
                if (undoneRecords[i].RemittanceImages.Count == 0)
                    return undoneRecords.ElementAt(i).RemittanceID;//tìm thấy chuyển khoản chưa hoàn thành
            }

            return -1;//ko có chuyển khoản chưa hoàn thành
        }

        public bool KiemTraQuyen(int userRoleID, int minRole)
        {
            if (userRoleID >= minRole) return true;
            return false;
        }

        public bool KiemTraQuyen(int userRoleID, int minRole, int maxRole)
        {
            if (userRoleID >= minRole && userRoleID <= maxRole) return true;
            return false;
        }

        public bool KiemTraDangNhap(int registeredID)
        {
            return registeredID > 0 ? true : false;
        }

        public bool KiemtraDuTienTrongTaiKhoan(int registeredID, int fund)
        {
            BankDBEntities db = new BankDBEntities();
            BankInfo info = db.BankInfoes.Find(registeredID);

            return info.Balance > fund ? true : false;
        }

        public int GetLatestLoanRecordID(LoanRecord record)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["BankDB"].ConnectionString);
            conn.Open();
            string query = "insert into LoanRecord(LoanReasonID) values(@LoanReasonID); Select scope_identity()";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@LoanReasonID", record.LoanReasonID);
            int id = Convert.ToInt32(cmd.ExecuteScalar());//lấy id của loanrecord
            conn.Close();

            return id;
        }
    }
}