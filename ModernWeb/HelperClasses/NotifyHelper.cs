﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ModernWeb.Models;
using System.IO;
using System.Net.Mail;

namespace ModernWeb.HelperClasses
{
    public class NotifyHelper
    {
        public void SendEmailToUserWithFailedTransfers()
        {
            List<RemittanceRecord> failedTransfers = loadFailedTransfers();
            sendEmailToFailedRemittanceUser(ref failedTransfers);
        }
        public void SendEmailToUserWithFailedLoans()
        {
            List<LoanRecord> failedLoans = loadFailedLoans();
            sendEmailToFailedLoanUser(ref failedLoans);
        }

        private void sendEmailToFailedRemittanceUser(ref List<RemittanceRecord> failedTransfers)
        {
            BankDBEntities db = new BankDBEntities();

            try
            {
                foreach (RemittanceRecord failedTransfer in failedTransfers)
                {
                    //send email
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress("12520781@aep.uit.edu.vn");
                    mail.To.Add(failedTransfer.RegisteredUser.PersonalInfo.Mail);
                    mail.Subject = "[Notification] Your bank order has been failed";
                    mail.Body = createMailTransferBody(failedTransfer);

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("12520781@aep.uit.edu.vn", "doxuanvinh17031994");
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);

                    //update status in db
                    db.RemittanceRecords.Single(r => r.RemittanceID == failedTransfer.RemittanceID).StatusID = 13;
                    db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                //Chua xong
                //StreamWriter myWrite = new StreamWriter(new System.Web.Mvc.UrlHelper().Content("~/logs/") + DateTime.Now + ".txt");
                //myWrite.WriteLine(ex.Message);
                //myWrite.Close();
            }
        }

        private void sendEmailToFailedLoanUser(ref List<LoanRecord> failedLoans)
        {
            BankDBEntities db = new BankDBEntities();

            try
            {
                foreach (LoanRecord failedLoan in failedLoans)
                {
                    //send email
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress("12520781@aep.uit.edu.vn");
                    mail.To.Add(failedLoan.BorrowerRecord.Email);
                    mail.Subject = "[Notification] Your bank Loan request has been rejected";
                    mail.Body = createMailLoanBody(failedLoan);

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("12520781@aep.uit.edu.vn", "doxuanvinh17031994");
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);

                    //update status in db
                    db.LoanRecords.Single(r => r.LoanRecordID == failedLoan.LoanRecordID).StatusID = 13;//đã gửi thành công
                    db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                //Chua xong
                //StreamWriter myWrite = new StreamWriter(new System.Web.Mvc.UrlHelper().Content("~/logs/") + DateTime.Now + ".txt");
                //myWrite.WriteLine(ex.Message);
                //myWrite.Close();
            }
        }

        private string createMailTransferBody(RemittanceRecord failedTransfer)
        {
            string body = "Modern Life Banking\n{0}\nDear {1},\n" +
                "We reget to announce the cancellation of your transfer order.\n" +
                "Details of your transfer order:\n" +
                "\t- Remittance Code: {2}\n" + "\t- Remittance Date: {3}\n" +
                "\t- Funds Transfered: {4}\n" + "\t- Receiver's Bank Number: {5}\n" +
                "\t- Received Bank's Name: {6}\n" +
                "Please contact us for more information.\n" +
                "Sincerely,\nBank Director";

            body = string.Format(body, DateTime.Now.ToShortDateString(), failedTransfer.RegisteredUser.PersonalInfo.FullName,
                failedTransfer.RemittanceID, failedTransfer.RemittanceDate.ToShortDateString(), failedTransfer.FundsTransfer.ToString(),
                failedTransfer.ReceiverBankNumber, failedTransfer.AssociatedBank.BankName);

            return body;
        }

        private string createMailLoanBody(LoanRecord failedLoan)
        {
            string body = "Modern Life Banking\n{0}\nDear {1},\n" +
                "We reget to announce the rejection of your loan request.\n" +
                "Details of your request:\n" +
                "\t- Loan Code: {2}\n" + "\t- Loan Date: {3}\n" +
                "\t- Interest amount of money: {4}\n" + "\t- Loan Reason: {5}\n" +
                "Please contact us for more information.\n" +
                "Sincerely,\nBank Director";

            body = string.Format(body, DateTime.Now.ToShortDateString(), failedLoan.BorrowerRecord.Name,
                failedLoan.LoanRecordID, failedLoan.LoanDate.ToString(), failedLoan.InterestValue.ToString(),
                failedLoan.LoanReason);

            return body;
        }

        private List<RemittanceRecord> loadFailedTransfers()
        {
            BankDBEntities db = new BankDBEntities();
            return db.RemittanceRecords.Where(r => r.StatusID == 5).ToList();
        }

        private List<LoanRecord> loadFailedLoans()
        {
            BankDBEntities db = new BankDBEntities();
            return db.LoanRecords.Where(r => r.StatusID == 8).ToList();
        }
    }
}