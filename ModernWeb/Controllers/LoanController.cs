﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModernWeb.Models;
namespace ModernWeb.Controllers
{
    public class LoanController : Controller
    {
        public ActionResult Index()
        {
            return View("Index");
        }

        [HttpGet]
        public ActionResult ChooseTypeOfLoan()
        {
            if (Session["appendingLoanTypeChoosing"] != null)
                return RedirectToAction("PersonalInfoSubmit", "Loan");

            LoanRecord record = new LoanRecord();
            var reasons = new BankDBEntities().LoanReasons.Select(l => new SelectListItem() { Value = l.LoanReasonID.ToString(), Text = l.LendReason });
            ViewData["reasons"] = new SelectList(reasons, "Value", "Text");
            return View(record);
        }

        [HttpPost]
        public ActionResult SubmitType(LoanRecord record)
        {
            if (!ModelState.IsValid)
                return View("ChooseTypeOfLoan", record);

            //lưu tạm dữ liệu kiểu cho vay
            Session["appendingLoanTypeChoosing"] = record;

            return RedirectToAction("PersonalInfoSubmit", "Loan");
        }

        [HttpGet]
        public ActionResult PersonalInfoSubmit()
        {
            if (Session["appendingLoanPersonalInfo"] != null)
            {
                return RedirectToAction("UploadLoanForm", "Upload");
            }

            if (Session["appendingLoanTypeChoosing"] == null)
                return RedirectToAction("ChooseTypeOfLoan", "Loan");

            BorrowerRecord record = new BorrowerRecord();
            return View(record);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PersonalInfoSubmit(BorrowerRecord record)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Bad input");
                return View("PersonalInfoSubmit", record);
            }

            //lưu tạm thông tin cá nhân
            Session["appendingLoanPersonalInfo"] = record;

            return RedirectToAction("UploadLoanForm", "Upload");
        }
    }
}
