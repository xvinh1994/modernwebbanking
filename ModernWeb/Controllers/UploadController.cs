﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModernWeb.Models;
using ModernWeb.HelperClasses;
using System.IO;

namespace ModernWeb.Controllers
{
    public class UploadController : Controller
    {
        public ActionResult UploadRemittanceForm()
        {
            int roleID = (int)Session["roleID"];
            if (roleID == 0)
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            int registeredID = (int)Session["registeredID"];
            if (registeredID == 0)
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            if ((bool)Session["transferFormChecked"] == false)
                return RedirectToAction("TransferMoney", "Account");

            return View("UploadRemittanceForm");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadRemittanceImage(IEnumerable<HttpPostedFileBase> files)
        {
            int roleID = (int)Session["roleID"];
            if (roleID == 0)
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            int registeredID = (int)Session["registeredID"];
            if (registeredID == 0)
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            try
            {
                foreach (var file in files)
                {
                    if (file.ContentLength > 0)
                    {
                        var filename = Path.GetFileName(file.FileName);
                        var physPath = "~/Upload/RemittanceImg/" + filename;
                        var path = Path.Combine(Server.MapPath("~/Upload/RemittanceImg/"), filename);

                        file.SaveAs(path);

                        BankDBEntities db = new BankDBEntities();
                        int remittanceID = (int)Session["curRemittanceID"];
                        db.RemittanceImages.Add(new RemittanceImage() { RemittanceID = remittanceID, ImageURL = physPath });
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", "No image chosen");
                return View("UploadRemittanceForm");
            }

            Session["curRemittanceID"] = null;
            Session["transferFormChecked"] = false;

            return RedirectToAction("TransferMoney", "Account");
        }

        public ActionResult UploadLoanForm()
        {
            if (Session["appendingLoanTypeChoosing"] == null)
                return RedirectToAction("ChooseTypeOfLoan", "Loan");

            if (Session["appendingLoanPersonalInfo"] == null)
            {
                return RedirectToAction("PersonalInfoSubmit", "Loan");
            }

            return View("UploadLoanForm");
        }

        [HttpPost]
        public ActionResult UploadLoanImage(IEnumerable<HttpPostedFileBase> files)
        {
            if (files.Count() <= 0)
            {
                ModelState.AddModelError("Error", "No image has been chosen");
                return View("UploadRemittanceForm", "Upload");
            }

            //lấy data đã lưu tạm
            LoanRecord lRecord = (LoanRecord)Session["appendingLoanTypeChoosing"];
            BorrowerRecord bRecord = (BorrowerRecord)Session["appendingLoanPersonalInfo"];

            //lưu vào db
            DBHelper helper = new DBHelper();
            int id = helper.GetLatestLoanRecordID(lRecord);

            BankDBEntities db = new BankDBEntities();
            bRecord.LoanRecordID = id;
            db.LoanRecords.Add(lRecord);
            db.BorrowerRecords.Add(bRecord);
            db.SaveChanges();

            try
            {
                foreach (var file in files)
                {
                    if (file.ContentLength > 0)
                    {
                        var filename = Path.GetFileName(file.FileName);
                        var physPath = "~/Upload/LoanImg/" + filename;
                        var path = Path.Combine(Server.MapPath("~/Upload/LoanImg/"), filename);

                        file.SaveAs(path);

                        db.LoanImages.Add(new LoanImage() { ImageURL = physPath, LoanRecordID = id });
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", "No image chosen");
                return View("UploadLoanForm");
            }

            Session["appendingLoanTypeChoosing"] = null;
            Session["appendingLoanPersonalInfo"] = null;

            return RedirectToAction("Index", "Loan");
        }
    }
}
