﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ModernWeb.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult News()
        {
            return View("News");
        }

        public ActionResult Services()
        {
            return View("Services");
        }

        public ActionResult Contact()
        {
            return View("Contact");
        }
    }
}
