﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModernWeb.Models;
using ModernWeb.HelperClasses;

namespace ModernWeb.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login(string returnURL)
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (checker.KiemTraQuyen(roleID, 1) && checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            ViewBag.ReturnURL = returnURL;
            return View("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(RegisteredUser user, string returnURL)
        {
            try
            {
                BankDBEntities db = new BankDBEntities();
                RegisteredUser loginUser = null;
                if ((loginUser = db.RegisteredUsers.Single(u => u.Username == user.Username && u.Password == user.Password)) == null)//no login
                {
                    ModelState.AddModelError("Error", "Login failed");
                    return View(user);
                }
                else
                {
                    Session["roleID"] = loginUser.Role.RoleID;
                    Session["registeredID"] = loginUser.RegisteredID;
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(user);
            }

            if (string.IsNullOrEmpty(returnURL) || string.IsNullOrWhiteSpace(returnURL))
                return RedirectToAction("Index", "Home");
            else return Redirect(returnURL);
        }

        public ActionResult Details()
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 1) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            BankDBEntities db = new BankDBEntities();
            RegisteredUser user = db.RegisteredUsers.Find(registeredID);

            return View(user);
        }

        public ActionResult Logout()
        {
            Session["roleID"] = 0;
            Session["registeredID"] = 0;

            return RedirectToAction("Index", "Home");
        }

        public ActionResult TransferHistory()
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 1) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            BankDBEntities db = new BankDBEntities();
            List<RemittanceRecord> history = db.RemittanceRecords.Where(r => r.RegisteredID == registeredID).OrderByDescending(r => r.RemittanceDate).ToList();

            return View(history);
        }

        [HttpGet]
        public ActionResult TransferMoney()
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 1) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            //ktra có chuyển khoản nào chưa hoàn tất
            //giả sử chỉ có 1 chuyển khoản chưa đc hoàn tất. e.g: đang làm cúp điện, ...
            Session["curRemittanceID"] = checker.KiemTraChuyenKhoanGianDoan(registeredID);
            if ((int)Session["curRemittanceID"] > 0)
                Session["transferFormChecked"] = true;

            //ktra form chuyển khoản có đang làm giữa chừng ko? e.g: đang làm đi ra ngoài
            if ((bool)Session["transferFormChecked"] == true)
                return RedirectToAction("UploadRemittanceForm", "Upload");

            return View("TransferMoney");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TransferMoney(RemittanceRecord record)
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 1) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            //ktra account có đủ tiền ko?
            if (!checker.KiemtraDuTienTrongTaiKhoan(registeredID, record.FundsTransfer))
            {
                ViewBag.SuccessMessage = "Không đủ tiền trong tài khoản";
                return View(record);
            }

            try
            {
                BankDBEntities db = new BankDBEntities();
                record.RegisteredID = registeredID;
                record.SenderBankNumber = db.BankInfoes.Find(registeredID).BankNumber;
                record.RemittanceDate = DateTime.Now;
                record.StatusID = 1;
                RemittanceRecord added = db.RemittanceRecords.Add(record);
                db.SaveChanges();

                Session["curRemittanceID"] = added.RemittanceID;
                Session["transferFormChecked"] = true;
            }
            catch (Exception ex)
            {
                ViewBag.SuccessMessage = "Có lỗi xảy ra. Thông tin lỗi: " + ex.Message;
                return View(record);
            }

            ViewBag.SuccessMessage = "Giao dịch đã được lưu lại";
            return RedirectToAction("UploadRemittanceForm", "Upload");
        }
    }
}
