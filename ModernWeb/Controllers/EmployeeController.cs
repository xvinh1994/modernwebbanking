﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModernWeb.HelperClasses;
using ModernWeb.Models;
using ModernWeb.AttributeClass; 

namespace ModernWeb.Controllers
{
    public class EmployeeController : Controller
    {
        public ActionResult Index()
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 1) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            return View();
        }

        public ActionResult CheckTransferList()
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 2) || !checker.KiemTraDangNhap(registeredID))
            {
                Session["roleID"] = 0;
                Session["registeredID"] = 0;
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });
            }

            BankDBEntities db = new BankDBEntities();
            List<RemittanceRecord> records = db.RemittanceRecords.Where(r => r.StatusID == 1).ToList();

            return View("CheckTransferList", records);
        }

        public ActionResult TransferCheckingForm(int id)
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 2) || !checker.KiemTraDangNhap(registeredID))
            {
                Session["roleID"] = 0;
                Session["registeredID"] = 0;
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });
            }

            RemittanceRecord r = new BankDBEntities().RemittanceRecords.Find(id);

            //ktra chuyển khoản đó đã duyệt info chưa
            if (r.StatusID != 1)
            {
                return View("CheckTransferList");
            }

            TempData["CheckingRemittanceID"] = id;
            return View("TransferCheckingForm", r);
        }

        [MultipleButton(Name = "action", Argument = "AllowTransfer")]
        [HttpPost]
        public ActionResult AllowTransfer()
        {
            int id = (int)TempData["CheckingRemittanceID"];
            BankDBEntities db = new BankDBEntities();
            db.RemittanceRecords.Find(id).StatusID = 2;
            db.SaveChanges();

            return RedirectToAction("CheckTransferList", "Employee");
        }

        [MultipleButton(Name = "action", Argument = "DenyTransfer")]
        [HttpPost]
        public ActionResult DenyTransfer()
        {
            int id = (int)TempData["CheckingRemittanceID"];
            BankDBEntities db = new BankDBEntities();
            db.RemittanceRecords.Find(id).StatusID = 5;
            db.SaveChangesAsync();

            return RedirectToAction("CheckTransferList", "Employee");
        }

        public ActionResult CheckLoanList()
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 1, 3) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            BankDBEntities db = new BankDBEntities();
            List<LoanRecord> records = db.LoanRecords.Where(l => l.StatusID == 1).ToList();//chưa duyệt thông tin

            return View("CheckLoanList", records);
        }

        public ActionResult GetLoanDetails(int id)
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 1) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            LoanRecord r = new BankDBEntities().LoanRecords.Find(id);

            if (r.StatusID != 1)
                return View("CheckLoanList");

            TempData["CheckingLoanID"] = id;
            return View("GetLoanDetails", r);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "LoanInfoOK")]
        public ActionResult LoanInfoOK()
        {
            int id = (int)TempData["CheckingLoanID"];
            BankDBEntities db = new BankDBEntities();
            db.LoanRecords.Find(id).StatusID = 6;//chờ duyệt để nhân tư vấn đi gặp khách hàng
            db.SaveChanges();

            return RedirectToAction("CheckLoanList", "Employee");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "LoanInfoFailed")]
        public ActionResult LoanInfoFailed()
        {
            int id = (int)TempData["CheckingLoanID"];
            BankDBEntities db = new BankDBEntities();
            db.LoanRecords.Find(id).StatusID = 7;
            db.SaveChanges();

            return RedirectToAction("CheckLoanList", "Employee");
        }

        public ActionResult WaitingAdvisoryList()
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 2, 2) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            BankDBEntities db = new BankDBEntities();
            List<LoanRecord> records = db.LoanRecords.Where(l => l.StatusID == 6).ToList();

            return PartialView(records);
        }

        public ActionResult AcceptAdvisory(int id)
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 2, 2) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            BankDBEntities db = new BankDBEntities();
            db.LoanRecords.Find(id).StatusID = 6;

            AssigneeAdvisory advisory = new AssigneeAdvisory() { LoanRecordID = id, RegisteredID = registeredID };
            db.AssigneeAdvisories.Add(advisory);

            db.SaveChanges();

            return View("WaitingAdvisoryList");
        }

        public ActionResult AdvisoryHistory()
        {
            DBHelper checker = new DBHelper();
            int roleID = (int)Session["roleID"];
            int registeredID = (int)Session["registeredID"];

            if (!checker.KiemTraQuyen(roleID, 2, 2) || !checker.KiemTraDangNhap(registeredID))
                return RedirectToAction("Login", "Account", new { ReturnURL = Request.Url.AbsolutePath });

            BankDBEntities db = new BankDBEntities();
            List<AssigneeAdvisory> history = db.AssigneeAdvisories.Where(a => a.RegisteredID == registeredID).ToList();

            return View("AdvisoryHistory", history);
        }
    }
}
