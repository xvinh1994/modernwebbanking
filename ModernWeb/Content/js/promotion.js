﻿$(document).ready(function(e) {
    //popup promotion
    $('a.open_promotion').click(function(e) {
        e.preventDefault();
        var request = $.ajax({
            url: $(this).data('url') + "&lang=" + $("meta[name='lang']").attr('content'),
            type: "GET"
        });
        request.done(function(result) {
            $('.popinner').html(result);
            showpopup('mainpopup', { width: '870px', height: '510px' });
                setTimeout(function () {
                    $('.promotion_popup .jCarouselLite').jCarouselLite({
                        btnNext: '.next',
                        btnPrev: '.prev',
                        visible: 4,
                        scroll: 1,
                        start: 0
                    });
                }, 500);
                
        });
        request.fail(function(jqXHR, textStatus) {
            //alert("Request failed: " + textStatus);
        });
        return false;
    });
});
/* promotion */
function showMerchantFilterBar(cityId, districtId, strSearch) {
    strSearch = encodeURIComponent(strSearch);
    var request = $.ajax({
        url: "/common/showpromotionfilter?cityId=" + cityId + "&districtId=" + districtId + "&strSearch=" + strSearch,
        type: "GET"
    });
    request.done(function (result) {
        $('#filterContainer').html(result);
    });
    request.fail(function (jqXHR, textStatus) {
        //alert("Request failed: " + textStatus);
    });
}

/* atm-branches */
function showBranchesFilterBar(cityId, districtId, strSearch, chkBranch, chkATM, chkPriority) {
    strSearch = encodeURIComponent(strSearch);
    var request = $.ajax({
        url: "/common/showbranchesfilter?fCityId=" + cityId + "&fDistrictId=" + districtId + "&fKeyword=" + strSearch + "&chkBranch=" + chkBranch + "&chkAtm=" + chkATM + "&chkPriority=" + chkPriority,
        type: "GET"
    });
    request.done(function (result) {
        $('#filterContainer').html(result);
    });
    request.fail(function (jqXHR, textStatus) {
        //alert("Request failed: " + textStatus);
    });
}

function showPriorityFilterBar(cityId, districtId, strSearch) {
    strSearch = encodeURIComponent(strSearch);
    var request = $.ajax({
        url: "/common/showpriorityfilter?fCityId=" + cityId + "&fDistrictId=" + districtId + "&fKeyword=" + strSearch,
        type: "GET"
    });
    request.done(function (result) {
        $('#filterContainer').html(result);
    });
    request.fail(function (jqXHR, textStatus) {
        //alert("Request failed: " + textStatus);
    });
}

function RedirectToPage(baseUrl, id) {
    window.location = baseUrl + id;
}

