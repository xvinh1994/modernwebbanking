jQuery.validator.addMethod("alphabet", function (value, element) {
    return this.optional(element) || /^[\w\s]$/.test(value);
});
jQuery.validator.addMethod("mustselect", function (value, element, arg) {
    return $(element).get(0).value != "";
});
jQuery.validator.addMethod("phone", function (value, element) {
    value = value.replace(/\s+/g, "");
    return this.optional(element) || value.length > 7 && value.match(/^[\d\s\(\)\.\-\+]+$/);
});
jQuery.validator.addMethod("checkboxes_required", function (value, element) {
    var tr = $(element).parents("tr:first");
    return !$(tr).is(':visible') || $(tr).find("input:checked").length > 0;
});
jQuery.validator.addMethod("radio_required", function (value, element) {
    var tr = $(element).parents("tr:first");
    return !$(tr).is(':visible') || $(tr).find("input:checked").length > 0;
});
jQuery.validator.addMethod("email", function (value, element) {
    return this.optional(element) || /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/.test(value);
});

var validate_lang = 'vi';
var validate_message_format = {
    'vi':{
        'required': 'Vui lòng nhập {0}',
        'mustselect': 'Vui lòng chọn {0}',
        'url':'{0} phải đúng chuẩn đường dẫn',
        'email':'{0} phải đúng chuẩn email',
        'date': '{0} phải đúng chuẩn ngày tháng',
        'digits': '{0} chỉ được nhập số',
        'creditcard': '{0} phải đúng chuẩn số thẻ',
        'phone': '{0} phải đúng chuẩn số điện thoại',
        'alphabet': '{0} chỉ được nhập chữ cái',
        'checkboxes_required' : 'Vui lòng check chọn {0}'
    },
    'en': {
        'required': 'Please input {0}',
        'mustselect': 'Please select {0}',
        'url': '{0} must be a url',
        'email': '{0} must be a email',
        'date': '{0} must be a date',
        'digits': '{0} only input number',
        'creditcard': '{0} must be a credit card number',
        'phone': '{0} must be a phone number',
        'alphabet': '{0} only input letters of the alphabet',
        'checkboxes_required': 'Please check options {0}'
    }
};

function validateGeneratedForm() {
    $('.generated_form').each(function (f, form) {
        var validate_options = {
            rules: {},
            messages: {},
            errorElement: "div",
            errorPlacement: function (error, element) {
                element.parents("td:first").append(error);
            }
        };
        $(form).find('input[data-validatetags], select[data-validatetags], textarea[data-validatetags]').each(function (i, element) {
            var parent = $(element).parent().parent();
            var inputname = $.trim($(parent).find('td:first').text().replace(/[\s\r\n\*]+/g, ' '));
            //var tags = $(element).data('validatetags').split(',');
            var tags = $(element).attr('data-validatetags').split(',');
            validate_options.rules[$(element).attr('id')] = {};
            validate_options.messages[$(element).attr('id')] = {};
            //console.log(tags);
            for (var i in tags) {
                if ($.trim(tags[i]) == '')
                    continue;
                if (tags[i] == 'required' && element.tagName.toLowerCase() == 'select')
                    tags[i] = 'mustselect';
                
                validate_options.rules[$(element).attr('id')][tags[i]] = true;
                var msg = validate_message_format[validate_lang][tags[i]].replace('{0}', inputname.toLowerCase());
                var capitalized = msg.charAt(0).toUpperCase() + msg.substring(1);
                validate_options.messages[$(element).attr('id')][tags[i]] = capitalized;
            }
        });
        //console.log(validate_options);
        $(form).validate(validate_options);
    });
}

function validateElement(element) {
    var valid = true;
    var tr = $(element).parents("tr:first");
    if ($(tr).is(':visible')) {
        if (!$(this).is(':visible') && $(this).data('validatetags') != 'checkboxes_required') {
            valid = true;
        } else {
            valid = $(element).valid();
        }
    }
    return valid;
}

$(document).ready(function (e) {
    if($('meta[name="lang"]').length>0){
        validate_lang = $('meta[name="lang"]').attr('content');
    }

    validateGeneratedForm();

    // check validate with input hidden has datavalidatetag
    $('.generated_form').submit(function () {
        var valid = true;
        $('.generated_form *[data-validatetags]').each(function (i, el) {
            valid &= validateElement(el);
        });
        if (!valid) return false;
    });

    $('.generated_form .tab a, .form_footer a').click(function (e) {
        
        var valid = true;
        if ($(this).attr('rel') == 'form_step2') {
            $('.generated_form .form_step1 *[data-validatetags]').each(function (i1, el1) {
                //valid &= $(el1).valid();
                valid &= validateElement(el1);
            });
        }
        if ($(this).attr('rel') == 'form_step3') {
            $('.generated_form .form_step1 *[data-validatetags]').each(function (i1, el1) {
                //valid &= $(el1).valid();
                valid &= validateElement(el1);
            });
            if(valid) {
                $('.generated_form .form_step2 *[data-validatetags]').each(function (i2, el2) {
                    //valid &= $(el2).valid();
                    valid &= validateElement(el2);
                });
            }
        }
        if (valid) {
            $('.generated_form .form_step1, .generated_form .form_step2, .generated_form .form_step3').hide();
            $('.generated_form .' + $(this).attr('rel')).show();
        }
    });
});