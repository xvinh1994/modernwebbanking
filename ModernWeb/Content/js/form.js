

function hideblock(obj) {
    var trTags = $(obj).nextAll('tr');
    var firstEndTagIndex = $(trTags).index(trTags.filter('.endblock').first());
    for (i = 0; i < firstEndTagIndex; i++) {
        $(trTags[i]).hide();
        $(trTags[i]).find("*[type!='hidden']").hide();
    }
}

function showBlock(obj) {
    var trTags = $(obj).nextAll('tr');
    var firstEndTagIndex = $(trTags).index(trTags.filter('.endblock').first());
    for (i = 0; i < firstEndTagIndex; i++) {
        $(trTags[i]).show();
        $(trTags[i]).find("*[type!='hidden']").show();
    }
}

function resetBlock(obj) {
    var trTags = $(obj).nextAll('tr');
    var firstEndTagIndex = $(trTags).index(trTags.filter('.endblock').first());
    for (i = 0; i < firstEndTagIndex; i++) {
        $(trTags[i]).find("input[type='text']").val('');
        $(trTags[i]).find("input[type='checkbox']").attr("checked", false);
        $(trTags[i]).find("input[type='radio']").attr("checked", false);
        $(trTags[i]).find("input[type='select']").prop('selectedIndex', 0);
        // clear hidden value of input tương ứng
        $(trTags[i]).find("input[type='hidden']").each(function () {
            var defaultVal = $(this).data('defaultvalue');
            if (defaultVal != undefined) {
                $(this).val(defaultVal);
            }
        });
    }
}


$(document).ready(function (e) {
    var lang = $("meta[name='lang']").attr('content');
    var date_format = lang.toLowerCase() == "vi" ? "dd/mm/yy" : "mm/dd/yy";
    //indent paragraph 
    $('form.generated_form').find('tr[data-indentparagraph="True"]').each(function () {
        var innerhtml = $(this).html();
        var html = "<td></td><td><table>" + innerhtml + "</table></td>";
        $(this).html(html);
    });

    // Handle form events
    $('form.generated_form').submit(function (e) {
        $(this).find('input[name="__return"]').val(window.location.href);
        //if($("input.captcha-input")) {
        //    var tdcaptcha = $("input.captcha-input").parents('td:first');
        //    if (tdcaptcha != null && $(tdcaptcha).find('.captcha-error').length > 0) {
        //        return false;
        //    } 
        //}
    });

    //$("input.captcha-input").keyup(function () {
    //    var tdcaptcha = $(this).parents('td:first');
    //    var captcha = $(this).val();
    //    if (captcha.length == 8 || $(tdcaptcha).find('.captcha-error').length == 0) {
    //        var request = $.ajax({
    //            url: '/ajax/checkcaptcha?captcha=' + captcha,
    //            type: "GET",
    //            dataType: "json"
    //        });
    //        request.done(function (result) {
    //            if (result.Result == false) {
    //                //$("#captcha-image").html('<img style="border: 1px #333 solid;" src="/common/captcha?str=' + toSlug(Date()) + '">');
    //                //$(this).val('');
    //                if ($(tdcaptcha).find('.captcha-error').length == 0) $(tdcaptcha).append('<div class="captcha-error">' + result.Message + '</div>');
    //            } else {
    //                $(tdcaptcha).find('.captcha-error').each(function () {
    //                    $(this).remove(".captcha-error");
    //                });
    //            }
    //        });
    //        request.fail(function (jqXHR, textStatus) {
    //            //alert("Request failed: " + textStatus);
    //        });
    //    }else if(captcha.length==0) {
    //        $(tdcaptcha).find('.captcha-error').each(function () {
    //            $(this).remove(".captcha-error");
    //        });
    //    }
    //});
    
    // Setup form elements
    $('.form-daymonthyear').each(function (i, e) {
        var day = $('<select class="form-day"></select>');
        for (i = 1; i <= 31; i++) {
            day.append($('<option value="' + i + '">' + (i > 9 ? i : '0' + i) + '</option>'));
        }
        var month = $('<select class="form-month"></select>');
        if (lang.toLowerCase() == "vi") {
            for (i = 1; i <= 12; i++) {
                month.append($('<option value="' + i + '">Tháng ' + (i > 9 ? i : '0' + i) + '</option>'));
            }
        } else {
            var arrmonth = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            for (var j = 0; j < arrmonth.length; j++) {
                month.append($('<option value="' + arrmonth[j] + '">' + arrmonth[j] + '</option>'));
            }
        }

        var year = $('<select class="form-year"></select>');
        var date = new Date();
        for (i = date.getFullYear() ; i >= date.getFullYear() - 100 ; i--) {
            year.append($('<option value="' + i + '">' + i + '</option>'));
        }
        $(e).parent().prepend(year);
        $(e).parent().prepend(month);
        $(e).parent().prepend(day);
        $(e).hide();
        $(e).parent().find('select').change(function (el) {
            $(this).parent().find('input[type="hidden"]').val($(this).parent().find('select.form-day').val() + '/' + $(this).parent().find('select.form-month').val() + '/' + $(this).parent().find('select.form-year').val());
        });
        $(e).parent().find('select').change();
    });
    $('.form-datepicker').datepicker({ dateFormat: date_format, changeMonth: true, changeYear: true });
    $('.form-timepicker').each(function (i, e) {
        var hour = $('<select class="form-hour"></select>');
        for (i = 0; i <= 23; i++) {
            hour.append($('<option value="' + i + '">' + (i > 9 ? i : '0' + i) + '</option>'));
        }
        var min = $('<select class="form-min"></select>');
        for (i = 0; i <= 59; i++) {
            min.append($('<option value="' + i + '">' + (i > 9 ? i : '0' + i) + '</option>'));
        }
        $(e).parent().prepend(min);
        $(e).parent().prepend(hour);
        $(e).hide();
        $(e).parent().find('select').change(function (el) {
            $(this).parent().find('input[type="hidden"]').val($(this).parent().find('select.form-hour').val() + ':' + $(this).parent().find('select.form-hour').val());
        });
    });
    $('.form-phonenumber').each(function (i, e) {
        for (i = 2; i >= 1; i--) {
            $(e).parent().prepend('<input type="text" class="form-phoneno part' + i + '" />');
        }
        $(e).hide();
        $(e).parent().find('input[type="text"]').change(function (el) {
            var numbers = "";
            $(this).parent().find('input[type="text"]').each(function (i2, e2) {
                numbers += $(e2).val();
            });
            $(this).parent().find('input[type="hidden"]').val(numbers);
        });
    });
    $('.form-cardnumber').each(function (i, e) {
        for (i = 4; i >= 1; i--) {
            $(e).parent().prepend('<input type="text" class="form-cardno" />');
        }
        $(e).hide();
        $(e).parent().find('input[type="text"]').change(function (el) {
            var numbers = "";
            $(this).parent().find('input[type="text"]').each(function (i2, e2) {
                numbers += $(e2).val();
            });
            $(this).parent().find('input[type="hidden"]').val(numbers);
        });
    });
    $('.form-checkboxes').each(function (i, e) {
        $(e).parent().find('input[type="checkbox"]').change(function (el) {
            var values = "";
            $(this).parents("tr:first").find('input:checked').each(function () {
                values += $(this).val() + ",";
            });
            if (values != "") values = values.substring(0, values.length - 1);
            $(this).parents('td:first').find('.form-checkboxes').val(values);
            $(this).parents('td:first').find('.form-checkboxes').valid();
        });
    });
    $('.form-htmleditor').each(function (i, e) {
        __editors["editor_" + $(e).attr("id")] = CKEDITOR.replace($(e).attr("id"), {
            language: 'vi',
            toolbar:
            [
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] }
            ]
        });
    });

    $(".rdpicker_from").bind("change", function () {
        var rdppicker_to = $(this).siblings('.rdpicker_to')[0];
        if (rdppicker_to != null) {
            var rdpicker_value = $(this).siblings('.rdpicker_value')[0];
            //console.log(rdpicker_value);
            if (rdpicker_value != null) {
                $(rdpicker_value).val($(this).val() + " - " + $(rdppicker_to).val());
            }
        }
    });
    $(".rdpicker_to").bind("change", function () {
        var rdppicker_from = $(this).siblings('.rdpicker_from')[0];
        if (rdppicker_from != null) {
            var rdpicker_value = $(this).siblings('.rdpicker_value')[0];
            if (rdpicker_value != null) {
                $(rdpicker_value).val($(rdppicker_from).val() + " - " + $(this).val());
            }
        }
    });

    $(".rdpicker_from").trigger("change");


    $("input").attr('autocomplete', 'off');
    $('.generated_form input[type=text][class!=captcha-input], textarea').attr('autocomplete', 'on');

    $("form input[type='radio'].radiobox").bind("change", function () {
        var inputhidden = $(this).parents('tr:first').find("input[type='hidden'].radiobox");
        if (inputhidden != null) {
            $(inputhidden).val($(this).val());
        }
    });

    $("select[name^='depend_checkboxes_']").bind("change", function () {
        var index = $(this).find("option:selected").index();
        if (index >= 0) {
            var selected_value = $(this).find("option:selected").val();
            var total_selectoption = $(this).find("option").length;
            var arr_tr_checkboxes = $(this).parents("tr").nextAll("tr.depend_checkboxes_tr");
            if (arr_tr_checkboxes.length > 0) {
                for (var i = 0; i < total_selectoption; i++) {
                    $(arr_tr_checkboxes[i]).find("input[type='checkbox']").attr("checked", false); // clear all checked of checked in TR tags getted
                    $(arr_tr_checkboxes[i]).hide(); // hide all TR tags getted    
                }
                if (selected_value != "") // - in case: "Vui long chon" is active
                {
                    var expected_tr_checkbox = arr_tr_checkboxes[index];
                    $(expected_tr_checkbox).show(); // show this TR tag   
                }
            }
        }
    });
    $("select[name^='depend_select_box_topic_']").bind("change", function () {
        var index = $(this).find("option:selected").index();
        if (index >= 0) {
            var selected_value = $(this).find("option:selected").val();
            var total_selectoption = $(this).find("option").length;
            var arr_tr_select_boxes = $(this).parents("tr").nextAll("tr.depend_select_box_title_tr");
            if (arr_tr_select_boxes.length > 0) {
                for (var i = 0; i < total_selectoption; i++) {
                    $(arr_tr_select_boxes[i]).find("select").val(0); // clear all selected of selectbox in TR tags getted
                    $(arr_tr_select_boxes[i]).hide(); // hide all TR tags getted    
                }
                if (selected_value != "") // - in case: "Vui long chon" is active
                {
                    var expected_tr_select_box = arr_tr_select_boxes[index];
                    $(expected_tr_select_box).show(); // show this TR tag   
                }
            }
        }
    });
    $("select[name^='depend_checkboxes_']").change();
    $("select[name^='depend_select_box_topic_']").change();

    // depend block
    if ($("input[type='radio'][name^='depend_block_']") != null) {
        $("tr.beginblock").each(function () { // hide all blocks
            hideblock(this);
        });

        $("input[type='radio'][name^='depend_block_']").bind("change", function () {
            var radioName = $(this).attr('name');
            var radioButtons = $(this).parents('tr:first').find("input[type='radio'][name='" + radioName + "']");

            var selectedIndex = radioButtons.index(radioButtons.filter(':checked'));
            var beginBlocks = $(this).parents("tr:first").nextAll("tr.beginblock");
            $(beginBlocks).each(function () {
                hideblock(this);
                resetBlock(this);
            });

            showBlock(beginBlocks[selectedIndex]);
        });
    }

    if ($("input[type='checkbox'][name^='depend_block_']") != null) {
        $("tr.beginblock").each(function () { // hide all blocks
            hideblock(this);
        });

        $("input[type='checkbox'][name^='depend_block_']").bind("change", function () {
            var ctrChecked = $(this).is(':checked');
            var selectedIndex = $(this).data('index');
            var beginBlocks = $(this).parents("tr:first").nextAll("tr.beginblock");
            if (ctrChecked) {
                showBlock(beginBlocks[selectedIndex]);
            } else {
                resetBlock(beginBlocks[selectedIndex]);
                hideblock(beginBlocks[selectedIndex]);
            }

        });
    }

});
